# Lab7 - Rollbacks and reliability

## Extra 1

If a function fails after sucesful transaction, we need to create a possibility to safely repeat a transaction. One of techniques is to assign ID to each transaction. We need to store all succeeded transactions in a table in pairs with IDs. Client needs to check whether current ID already exists and if it is not, then a client repeats failed transaction.

## Extra 2

We can undo an operation in case of fail using inverse operation.
We can use lock to avoid race conditions.
We can apply operation only when the operation of external system was done.