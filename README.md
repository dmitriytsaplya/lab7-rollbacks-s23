# Lab7 - Rollbacks and reliability

## Extra 1

If a function fails after sucesful transaction, we need to create a possibility to safely repeat a transaction. One of techniques is to assign ID to each transaction. We need to store all succeeded transactions in a table in pairs with IDs. Client needs to check whether current ID already exists and if it is not, then a client repeats failed transaction.

## Extra 2

In the following scenario we can use a coordinator node in our system. It collects requests from nodes and makes sure that all other nodes are ready to make a commit. If any node fails, the coodrinator asks them to do a rollback. This approach is also called two-phase commit protocol.

There might be a case that this approach is not enough. E-commerce service can use saga pattern. Each node keeps track of local transaction and can make compensating transactions in case of a failure. Nodes communicate with each other to share their local transactions and trigger new local transactions in other nodes. 